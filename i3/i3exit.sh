#!/bin/sh
lock() {
    #i3lock -c 222222
    scrot /tmp/screen.png
    convert /tmp/screen.png -scale 5% -scale 2000% /tmp/screen.png

    # suspend message display
    notify-send "DUNST_COMMAND_PAUSE"

    i3lock -i /tmp/screen.png -n

    # resume message display
    notify-send "DUNST_COMMAND_RESUME"

    rm /tmp/screen.png
}

case "$1" in
    lock)
        lock
        ;;
    logout)
        i3-msg exit
        ;;
    suspend)
        #lock && systemctl supend
        systemctl supend
        ;;
    hibernate)
        #lock && systemctl hibernate
        systemctl hibernate
        ;;
    reboot)
        systemctl reboot
        ;;
    shutdown)
        systemctl poweroff
        ;;
    *)
        echo "Usage: $0 {lock|logout|suspend|hibernate|reboot|shutdown}"
        exit 2
esac

exit 0
